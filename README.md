# Días Negros Tetris!

This is just a learning exercise for me to learn the basic facts of Rust programming language. It's dedicated to my rock band Días Negros. Visit us in https://www.instagram.com/_diasnegros/

## Instructions

* **'a' >** Move part to the left
* **'d' >** Move part to the right
* **'w' >** Rotate part
* **'s' >** Fast down movement

---

## License

This software is licensed under License GPL-3.0-only.

    Copyright Joseba Martinez 2019  (josebam@protonmail.com).

---

## Dependencies

This program the following crates:

* cursive: Is used to draw the game in the console. Is build in top of ncurses
* rand: Random numbers generation lib. Is used to select the next part with uniform distribution (so don't blame me if I type is not generated as you need ;)
* rodio: The lib that makes the game play nice music;)

---

## Music

The song that is played is called "Marea Negra" and is property of Días Negros
