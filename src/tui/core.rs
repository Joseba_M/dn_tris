/*
* License GPL-3.0-only
*
* Copyright Joseba Martinez 2019 (josebam@protonmail.com)
*
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, with version 3 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

use rand::{thread_rng, Rng};
use std::cmp;

// Game parameteres
pub const X_SIZE: u8 = 12; // min value = 10
pub const Y_SIZE: u8 = 23; // min value = 20+2 (hidden)
const POINTS_LINE: usize = 1;

#[derive(Copy, Clone, Debug)]
pub struct Coor {
    pub x: u8,
    pub y: u8,
}

#[derive(Copy, Clone, Debug)]
struct Shape {
    r: [[Coor; 4]; 4],
}

// O
const TY_O: Shape = Shape {
    r: [
        [
            Coor { x: 0, y: 0 },
            Coor { x: 1, y: 0 },
            Coor { x: 0, y: 1 },
            Coor { x: 1, y: 1 },
        ],
        [
            Coor { x: 0, y: 0 },
            Coor { x: 1, y: 0 },
            Coor { x: 0, y: 1 },
            Coor { x: 1, y: 1 },
        ],
        [
            Coor { x: 0, y: 0 },
            Coor { x: 1, y: 0 },
            Coor { x: 0, y: 1 },
            Coor { x: 1, y: 1 },
        ],
        [
            Coor { x: 0, y: 0 },
            Coor { x: 1, y: 0 },
            Coor { x: 0, y: 1 },
            Coor { x: 1, y: 1 },
        ],
    ],
};

// Z
const TY_Z: Shape = Shape {
    r: [
        [
            Coor { x: 0, y: 1 },
            Coor { x: 1, y: 1 },
            Coor { x: 1, y: 0 },
            Coor { x: 2, y: 0 },
        ],
        [
            Coor { x: 0, y: 0 },
            Coor { x: 0, y: 1 },
            Coor { x: 1, y: 1 },
            Coor { x: 1, y: 2 },
        ],
        [
            Coor { x: 0, y: 1 },
            Coor { x: 1, y: 1 },
            Coor { x: 1, y: 0 },
            Coor { x: 2, y: 0 },
        ],
        [
            Coor { x: 0, y: 0 },
            Coor { x: 0, y: 1 },
            Coor { x: 1, y: 1 },
            Coor { x: 1, y: 2 },
        ],
    ],
};

// S
const TY_S: Shape = Shape {
    r: [
        [
            Coor { x: 0, y: 0 },
            Coor { x: 1, y: 0 },
            Coor { x: 1, y: 1 },
            Coor { x: 2, y: 1 },
        ],
        [
            Coor { x: 0, y: 2 },
            Coor { x: 0, y: 1 },
            Coor { x: 1, y: 1 },
            Coor { x: 1, y: 0 },
        ],
        [
            Coor { x: 0, y: 0 },
            Coor { x: 1, y: 0 },
            Coor { x: 1, y: 1 },
            Coor { x: 2, y: 1 },
        ],
        [
            Coor { x: 0, y: 2 },
            Coor { x: 0, y: 1 },
            Coor { x: 1, y: 1 },
            Coor { x: 1, y: 0 },
        ],
    ],
};

// J
const TY_J: Shape = Shape {
    r: [
        [
            Coor { x: 0, y: 0 },
            Coor { x: 1, y: 0 },
            Coor { x: 1, y: 1 },
            Coor { x: 1, y: 2 },
        ],
        [
            Coor { x: 0, y: 1 },
            Coor { x: 1, y: 1 },
            Coor { x: 2, y: 1 },
            Coor { x: 2, y: 0 },
        ],
        [
            Coor { x: 0, y: 0 },
            Coor { x: 0, y: 1 },
            Coor { x: 0, y: 2 },
            Coor { x: 1, y: 2 },
        ],
        [
            Coor { x: 0, y: 1 },
            Coor { x: 0, y: 0 },
            Coor { x: 1, y: 0 },
            Coor { x: 2, y: 0 },
        ],
    ],
};

// L
const TY_L: Shape = Shape {
    r: [
        [
            Coor { x: 0, y: 2 },
            Coor { x: 0, y: 1 },
            Coor { x: 0, y: 0 },
            Coor { x: 1, y: 0 },
        ],
        [
            Coor { x: 0, y: 0 },
            Coor { x: 1, y: 0 },
            Coor { x: 2, y: 0 },
            Coor { x: 2, y: 1 },
        ],
        [
            Coor { x: 0, y: 2 },
            Coor { x: 1, y: 2 },
            Coor { x: 1, y: 1 },
            Coor { x: 1, y: 0 },
        ],
        [
            Coor { x: 0, y: 0 },
            Coor { x: 0, y: 1 },
            Coor { x: 1, y: 1 },
            Coor { x: 2, y: 1 },
        ],
    ],
};

// T
const TY_T: Shape = Shape {
    r: [
        [
            Coor { x: 0, y: 1 },
            Coor { x: 1, y: 1 },
            Coor { x: 2, y: 1 },
            Coor { x: 1, y: 0 },
        ],
        [
            Coor { x: 0, y: 0 },
            Coor { x: 0, y: 1 },
            Coor { x: 0, y: 2 },
            Coor { x: 1, y: 1 },
        ],
        [
            Coor { x: 0, y: 0 },
            Coor { x: 1, y: 0 },
            Coor { x: 2, y: 0 },
            Coor { x: 1, y: 1 },
        ],
        [
            Coor { x: 1, y: 2 },
            Coor { x: 1, y: 1 },
            Coor { x: 1, y: 0 },
            Coor { x: 0, y: 1 },
        ],
    ],
};

// I
const TY_I: Shape = Shape {
    r: [
        [
            Coor { x: 0, y: 0 },
            Coor { x: 0, y: 1 },
            Coor { x: 0, y: 2 },
            Coor { x: 0, y: 3 },
        ],
        [
            Coor { x: 0, y: 0 },
            Coor { x: 1, y: 0 },
            Coor { x: 2, y: 0 },
            Coor { x: 3, y: 0 },
        ],
        [
            Coor { x: 0, y: 0 },
            Coor { x: 0, y: 1 },
            Coor { x: 0, y: 2 },
            Coor { x: 0, y: 3 },
        ],
        [
            Coor { x: 0, y: 0 },
            Coor { x: 1, y: 0 },
            Coor { x: 2, y: 0 },
            Coor { x: 3, y: 0 },
        ],
    ],
};

#[derive(Copy, Clone, Debug)]
pub struct Part {
    typ: Shape,
    pub pos: Coor,
    rot: usize,
}

impl Part {
    pub fn new() -> Part {
        Part {
            // create a new random part to show
            pos: Coor {
                x: (X_SIZE / 2) - 1,
                y: 0,
            },
            rot: thread_rng().gen_range(0, 4),
            typ: {
                match thread_rng().gen_range(0, 7) {
                    0 => TY_O,
                    1 => TY_Z,
                    2 => TY_S,
                    3 => TY_J,
                    4 => TY_L,
                    5 => TY_T,
                    6 => TY_I,
                    _ => TY_O,
                }
            },
        }
    }

    pub fn rotate(&mut self) {
        self.rot = {
            match &self.rot {
                0 => 1,
                1 => 2,
                2 => 3,
                3 => 0,
                _ => 0,
            }
        }
    }

    pub fn down(&mut self) {
        self.pos.y += 1;
    }

    pub fn right(&mut self) {
        self.pos.x += 1;
    }

    pub fn left(&mut self) {
        self.pos.x -= 1;
    }

    pub fn curr_shape(&self) -> &[Coor; 4] {
        &self.typ.r[self.rot]
    }

    fn next_rot(&self) -> &[Coor; 4] {
        match self.rot {
            0 => &self.typ.r[1],
            1 => &self.typ.r[2],
            2 => &self.typ.r[3],
            3 => &self.typ.r[0],
            _ => &self.typ.r[0],
        }
    }

    pub fn max_x(&self) -> u8 {
        cmp::max(
            cmp::max(self.curr_shape()[0].x, self.curr_shape()[1].x),
            cmp::max(self.curr_shape()[2].x, self.curr_shape()[3].x),
        )
    }

    pub fn max_x_next(&self) -> u8 {
        cmp::max(
            cmp::max(self.next_rot()[0].x, self.next_rot()[1].x),
            cmp::max(self.next_rot()[2].x, self.next_rot()[3].x),
        )
    }
    pub fn max_y_next(&self) -> u8 {
        cmp::max(
            cmp::max(self.next_rot()[0].y, self.next_rot()[1].y),
            cmp::max(self.next_rot()[2].y, self.next_rot()[3].y),
        )
    }
}

#[derive(Copy, Clone, Debug)]
pub struct Map {
    pub m: [[bool; X_SIZE as usize]; Y_SIZE as usize],
    pub points: usize,
    pub next: Part,
    pub curr: Part,
    pub game_end: bool,
}

impl Map {
    // PUBLIC:
    pub fn new() -> Map {
        Map {
            m: [[false; X_SIZE as usize]; Y_SIZE as usize],
            points: 0,
            next: Part::new(),
            curr: Part::new(),
            game_end: false,
        }
    }

    pub fn step(&mut self, mov: u8) {
        self.check_gravity();
        self.check_lines();
        self.game_end = self.check_end();

        match mov {
            1 => {
                if self.check_right() {
                    self.curr.right()
                }
            }
            2 => {
                if self.check_left() {
                    self.curr.left()
                }
            }
            3 => {
                if self.check_rot() {
                    self.curr.rotate()
                }
            }
            _ => self.curr.down(),
        };
    }

    // PRIVATE
    fn check_lines(&mut self) {
        let mut full_lines: usize = 0;

        for y in (0..self.m.len()).rev() {
            let mut i = 0;

            for x in 0..self.m[0].len() {
                if self.m[y][x] {
                    i += 1;
                };
            }
            
            if i >= X_SIZE {
                full_lines += 1;
                for z in (0..y).rev() {
                    self.m[z + 1] = self.m[z];
                }
            }
        }
        
        self.points += full_lines * POINTS_LINE;
    }

    fn check_end(&self) -> bool {
        for i in self.m[1].iter() {
            if *i {
                return true;
            }
        }
        false
    }

    fn check_gravity(&mut self) {
        let x = self.curr.pos.x;
        let y = self.curr.pos.y;
        let mut block_part = false;

        for i in self.curr.curr_shape().iter() {
            if i.y + y == Y_SIZE - 1 || self.m[(i.y + y) as usize + 1][(i.x + x) as usize] {
                block_part = true;
            }
        }

        if block_part {
            for i in self.curr.curr_shape().iter() {
                self.m[(i.y + y) as usize][(i.x + x) as usize] = true;
            }
            self.curr = self.next;
            self.next = Part::new();
        }
    }

    fn check_rot(&mut self) -> bool {
        let x = self.curr.pos.x;
        let y = self.curr.pos.y;
        if x + self.curr.max_x_next() >= X_SIZE - 1 || y + self.curr.max_y_next() >= Y_SIZE {
            return false;
        }

        for i in self.curr.next_rot().iter() {
            if self.m[(i.y + y) as usize][(i.x + x) as usize] {
                return false;
            }
        }
        true
    }

    fn check_left(&mut self) -> bool {
        let x = self.curr.pos.x;
        let y = self.curr.pos.y;
        if x <= 0 {
            return false;
        }
        for i in self.curr.curr_shape().iter() {
            if self.m[(y + i.y) as usize][(x + i.x - 1) as usize] {
                return false;
            }
        }
        true
    }

    fn check_right(&mut self) -> bool {
        let x = self.curr.pos.x;
        let y = self.curr.pos.y;
        if x + self.curr.max_x() >= X_SIZE - 1 {
            return false;
        }
        for i in self.curr.curr_shape().iter() {
            if self.m[(y + i.y) as usize][(x + i.x + 1) as usize] {
                return false;
            }
        }
        true
    }
}
