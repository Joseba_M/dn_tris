/*
* License GPL-3.0-only
*
* Copyright Joseba Martinez 2019 (josebam@protonmail.com)
*
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, with version 3 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

mod core;

use cursive::direction::Direction;
use cursive::event::{Event, EventResult};
use cursive::vec::Vec2;
use cursive::views::{Button, Dialog, LinearLayout, Panel};
use cursive::Cursive;
use cursive::Printer;

struct Game {
    map: core::Map,
}

impl Game {
    pub fn new() -> Game {
        Game {
            map: core::Map::new(),
        }
    }
}

impl cursive::view::View for Game {
    fn draw(&self, p: &Printer) {
        p.print((core::X_SIZE as usize + 4, 0), "Días Negros!");
        p.print_hline(
            (core::X_SIZE as usize + 2, core::Y_SIZE as usize + 1),
            17,
            "^",
        );
        p.print_box(
            (0, 0),
            (core::X_SIZE as usize + 2, core::Y_SIZE as usize + 2),
            true,
        );
        p.print((core::X_SIZE as usize + 4, 2), "Next:");
        p.print((core::X_SIZE as usize + 4, 12), "Points:");
        let tmp_str = format!("- {} -", self.map.points.to_string());
        p.print((core::X_SIZE as usize + 3, 14), &tmp_str);

        // Map drawing loop
        for j in 1..self.map.m.len() {
            for i in 0..self.map.m[j].len() {
                if self.map.m[j][i] {
                    p.print((i + 1, j + 1), "◼");
                }
            }
        }

        if self.map.game_end {
            p.print((core::X_SIZE as usize + 4, 4), "Game Over");
        } else {
            // next part printing
            for i in self.map.next.curr_shape().iter() {
                p.print(((core::X_SIZE + 3 + i.x) as usize, (4 + i.y) as usize), "◼");
            }

            // Current part printing
            for i in self.map.curr.curr_shape().iter() {
                p.print(
                    (
                        (self.map.curr.pos.x + i.x) as usize + 1,
                        (self.map.curr.pos.y + i.y) as usize + 1,
                    ),
                    "◼",
                );
            }
        }
    }

    fn take_focus(&mut self, _: Direction) -> bool {
        true
    }

    fn on_event(&mut self, event: Event) -> EventResult {
        if !self.map.game_end {
            match event {
                Event::Char('s') => self.map.step(0),
                Event::Char('d') => self.map.step(1),
                Event::Char('a') => self.map.step(2),
                Event::Char('w') => self.map.step(3),
                _ => (),
            };
            self.map.step(0);
        }
        EventResult::Consumed(None)
    }

    fn required_size(&mut self, _: Vec2) -> Vec2 {
        Vec2::new((core::X_SIZE as usize) + 20, (core::Y_SIZE as usize) + 2)
    }
}

pub fn show_test() {
    let mut siv = Cursive::default();

    siv.add_global_callback('q', |s| s.quit());
    siv.set_fps(1);

    siv.add_layer(
        Dialog::new()
            .title("DN Tris!")
            .padding((2, 2, 1, 1))
            .content(
                LinearLayout::vertical()
                    .child(Button::new_raw("    Play!     ", show_canvas))
                    .child(Button::new_raw("    Exit     ", |s| s.quit())),
            ),
    );
    siv.run();
}

fn show_canvas(siv: &mut Cursive) {
    let g = Game::new();

    siv.add_layer(
        Dialog::new()
            .title("DN Tris!")
            .content(LinearLayout::horizontal().child(Panel::new(g)))
            .button("Quit game", |s| {
                s.pop_layer();
            }),
    )
}
