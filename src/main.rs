/*
* License GPL-3.0-only
*
* Copyright Joseba Martinez 2019 (josebam@protonmail.com)
*
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, with version 3 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

/*
   All the content of tui.rs could have been made here, but wasn't
   because of learning purpouses.
*/

mod tui;

use std::fs::File;
use std::io::BufReader;
use rodio::Source;
use rodio::Sink;

fn main() {
	let device = rodio::default_output_device().unwrap();
	let sink = Sink::new(&device);
	let file = File::open("mf1.ogg").unwrap();
	let source = rodio::Decoder::new(BufReader::new(file)).unwrap();
	
	sink.append(source);
	
	sink.play();
	
    tui::show_test();

}
